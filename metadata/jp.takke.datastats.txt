Categories:Internet,Office
License:Apache2
Web Site:
Source Code:https://github.com/takke/DataStats
Issue Tracker:https://github.com/takke/DataStats/issues

Auto Name:DataStats
Summary:View bandwith usage
Description:
Simple bandwidth consumption viewer with configurable polling options.

[https://github.com/takke/DataStats/blob/HEAD/ChangeLog.txt Changelog]
.

Repo Type:git
Repo:https://github.com/takke/DataStats

Build:1.2.1,6
    commit=588e561f4ab14e8fb5f90eba17bf92a66a87a0a4
    subdir=app
    gradle=yes
    prebuild=true

Build:1.2.3,8
    commit=ed7420a8f8d4b8f273753a69be36c26bf2d66b71
    subdir=app
    gradle=yes
    prebuild=true

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.2.3
Current Version Code:8

