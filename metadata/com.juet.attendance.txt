Categories:Science & Education,Office
License:MIT
Web Site:
Source Code:https://github.com/mavidser/attendance-viewer
Issue Tracker:https://github.com/mavidser/attendance-viewer/issues

Auto Name:Attendance Viewer
Summary:Attendance tracker for Indian colleges
Description:
Students of JUET and JIIT-128 can view class attendance. It visualizes
the attendance in a streamlined manner, along with additional details
for each subject, providing details about the subject. You can also set
a threshold attendance which helps you in maintaining the attendance.
.

Repo Type:git
Repo:https://github.com/mavidser/attendance-viewer

Build:2.1.1,6
    commit=2c9586d2d3601ef02e42c2817fff9b4c1c474952
    srclibs=1:ActionBarSherlock@4.4.0
    extlibs=android/android-support-v4.jar
    prebuild=cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/
    rm=out,libs,gen,gradle,libraries,build.gradle,settings.gradle,gradlew,gradlew.bat,.idea

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.1.1
Current Version Code:6

