Categories:Phone & SMS
License:GPLv3+
Web Site:http://www.lumicall.org
Source Code:https://github.com/opentelecoms-org/lumicall
Issue Tracker:https://github.com/opentelecoms-org/lumicall/issues

Auto Name:Lumicall
Summary:SIP softphone
Description:
SIP softphone with a comprehensive range of features:
* ENUM dialing seamlessly checks all numbers you dial
* DNS SRV lookup keeps track of which contact email addresses are active for federated SIP
* TLS encryption of SIP messaging and both SRTP and ZRTP supported for audio stream encryption
* ICE/STUN/TURN algorithms for NAT traversal in virtually any network (based on ice4j from Jitsi)
* Push-to-talk (PTT) walkie-talkie mode allows free group chat on wifi (as long as the router supports multicast)
* Built in Ganglia agent provides an enterprise-grade monitoring solution to map wifi coverage and it's relationship with call quality
* Native x86 support

Note: We are currently in the process on removing remaining jar files from this app.
Please see the [https://f-droid.org/wiki/page/org.lumicall.android maintainer notes]
about the status.
.

Repo Type:git
Repo:https://github.com/opentelecoms-org/lumicall.git

Build:1.8.3,118
    commit=1.8.3
    submodules=yes
    buildjni=yes

Build:1.8.5,120
    commit=1.8.5
    submodules=yes
    buildjni=yes

Build:1.8.6,121
    commit=1.8.6
    submodules=yes
    buildjni=yes

Build:1.8.7,122
    commit=1.8.7
    submodules=yes
    buildjni=yes

Build:1.8.11,126
    disable=build hangs - attempting to use ssh github for one submodule
    commit=unknown - see disabled
    submodules=yes
    buildjni=yes

Build:1.8.13,128
    disable=build hangs - attempting to use ssh github for one submodule
    commit=unknown - see disabled
    submodules=yes
    buildjni=yes

Build:1.8.15,130
    disable=build hangs - attempting to use ssh github for one submodule
    commit=unknown - see disabled
    submodules=yes
    buildjni=yes

Build:1.8.18,133
    disable=build hangs - attempting to use ssh github for one submodule
    commit=unknown - see disabled
    submodules=yes
    buildjni=yes

Build:1.8.19,134
    commit=1.8.19
    submodules=yes
    buildjni=yes

Build:1.9.1,135
    commit=1.9.1
    submodules=yes
    buildjni=yes

Build:1.9.2,136
    commit=1.9.2
    submodules=yes
    buildjni=yes

Build:1.9.5,139
    commit=1.9.5
    submodules=yes
    buildjni=yes

Build:1.9.10,144
    commit=1.9.10
    submodules=yes
    prebuild=echo "project.target.apilevel=8" > ant.properties
    buildjni=yes

Build:1.9.11,145
    commit=1.9.11
    submodules=yes
    prebuild=echo "project.target.apilevel=8" > ant.properties
    buildjni=yes

Build:1.9.12,146
    commit=1.9.12
    submodules=yes
    prebuild=echo "project.target.apilevel=8" > ant.properties && \
        sed -i '/<dependency/,/\/>/d' custom_rules.xml
    buildjni=yes

Build:1.10.3,151
    commit=1.10.3
    submodules=yes
    prebuild=echo "project.target.apilevel=8" > ant.properties && \
        sed -i '/<dependency/,/\/>/d' custom_rules.xml
    buildjni=yes

Build:1.10.6,154
    disable=jars
    commit=1.10.6
    submodules=yes
    srclibs=libphonenumber@fd0b2a071b823b420e07a34720e99486038fe53b
    rm=libs/libphonenumber*
    prebuild=echo "project.target.apilevel=8" > ant.properties && \
        sed -i '/<dependency/,/\/>/d' custom_rules.xml && \
        pushd $$libphonenumber$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$libphonenumber$$/target/*jar libs/
    buildjni=yes

Build:1.11.6,161
    disable=builds, submodules have jars
    commit=1.11.6
    submodules=yes
    srclibs=dnsjava@dnsjava-2.1.6,RemoteTea@release-1.1.0,libphonenumber@53794af89c54549b71a3db1f326e1bfd7c914832
    rm=libs/*.jar
    prebuild=echo "project.target.apilevel=8" > ant.properties && \
        sed -i '/<dependency/,/\/>/d' custom_rules.xml && \
        pushd $$dnsjava$$ && \
        ant jar && \
        popd && \
        cp $$dnsjava$$/dnsjava-*.jar libs/ && \
        pushd $$RemoteTea$$ && \
        ant -Dant.build.javac.target=1.5 -Dant.build.javac.source=1.5 jar && \
        popd && \
        cp $$RemoteTea$$/classes/oncrpc*jar libs/oncrpc-1.0.7.jar && \
        cp libs/oncrpc-1.0.7.jar gmetric4j/lib/oncrpc-1.0.7.jar && \
        pushd $$libphonenumber$$/java/ && \
        $$MVN3$$ package && \
        popd && \
        cp $$libphonenumber$$/java/libphonenumber/target/libphonenumber-6.2.3-SNAPSHOT.jar libs/
    buildjni=yes

Build:1.11.10,165
    disable=zrtp not building
    commit=1.11.10
    submodules=yes
    srclibs=PBKDF2@v1.1.0,dnsjava@dnsjava-2.1.6,RemoteTea@release-1.1.0,libphonenumber@libphonenumber-7.0.2
    rm=libs/*.jar
    extlibs=android/android-support-v4.jar
    prebuild=cp assets/app.properties-prod app.properties && \
        echo "project.target.apilevel=8" > ant.properties && \
        sed -i -e '/<dependency/,/\/>/d' custom_rules.xml && \
        mv zrtp zorg-java && \
        mkdir zrtp && \
        mv zorg-java zrtp/ && \
        pushd $$PBKDF2$$ && \
        gradle install && \
        popd && \
        cp $$PBKDF2$$/build/libs/PBKDF2-1.1.0.jar libs/ && \
        pushd $$dnsjava$$ && \
        ant jar && \
        popd && \
        cp $$dnsjava$$/dnsjava-*.jar libs/ && \
        pushd $$libphonenumber$$/java/ && \
        $$MVN3$$ package && \
        popd && \
        cp $$libphonenumber$$/java/libphonenumber/target/libphonenumber-7.0.2.jar libs/ && \
        pushd $$RemoteTea$$ && \
        ant -Dant.build.javac.target=1.5 -Dant.build.javac.source=1.5 jar && \
        popd && \
        cp $$RemoteTea$$/classes/oncrpc*jar libs/oncrpc-1.0.7.jar && \
        cp libs/oncrpc-1.0.7.jar gmetric4j/lib/oncrpc-1.0.7.jar
    buildjni=yes

Build:1.11.11,166
    commit=b77763126bbefa70868b17ce2195448dd5334360
    submodules=yes
    srclibs=PBKDF2@v1.1.0,dnsjava@dnsjava-2.1.6,RemoteTea@1.0.7,libphonenumber@libphonenumber-7.0.2,JUnit@r4.12,weupnp@weupnp-0.1.2
    rm=libs/*.jar,opentelecoms.org-util/lib/*.jar,gmetric4j/lib/*.jar,ice4j/lib/weupnp*.jar,ice4j/libs/junit.jar
    extlibs=android/android-support-v4.jar
    prebuild=cp assets/app.properties-prod app.properties && \
        echo "project.target.apilevel=8" > ant.properties && \
        pushd $$PBKDF2$$ && \
        gradle install && \
        popd && \
        cp $$PBKDF2$$/build/libs/PBKDF2-1.1.0.jar libs/ && \
        pushd $$dnsjava$$ && \
        ant jar && \
        popd && \
        cp $$dnsjava$$/dnsjava-2.1.6.jar libs/ && \
        cp libs/dnsjava-2.1.6.jar opentelecoms.org-util/lib/dnsjava-2.1.1.jar && \
        pushd $$libphonenumber$$/java/ && \
        $$MVN3$$ package && \
        popd && \
        cp $$libphonenumber$$/java/libphonenumber/target/libphonenumber-7.0.2.jar libs/ && \
        pushd $$RemoteTea$$ && \
        ant -Dant.build.javac.target=1.5 -Dant.build.javac.source=1.5 jar && \
        popd && \
        cp $$RemoteTea$$/classes/oncrpc*jar libs/oncrpc-1.0.7.jar && \
        cp libs/oncrpc-1.0.7.jar gmetric4j/lib/ && \
        pushd $$weupnp$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$weupnp$$/target/weupnp-0.1.2.jar ice4j/lib/weupnp-0.1.2-SNAPSHOT.jar && \
        pushd $$JUnit$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JUnit$$/target/junit-4.12.jar ice4j/lib/junit.jar && \
        cp ice4j/lib/junit.jar gmetric4j/lib/junit-4.1.jar
    buildjni=yes

Maintainer Notes:
* libs/jars:
    * jain-sdp (ice4j)
    * xalan (ice4j)
    * bcprov (zrtp)
    * zorg-bouncycastle (zrtp)
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.11.11
Current Version Code:166

