Categories:System
License:GPLv3
Web Site:https://gitlab.com/xphnx/twelf_cm12_theme/blob/HEAD/README.md
Source Code:https://gitlab.com/xphnx/twelf_cm12_theme
Issue Tracker:https://gitlab.com/xphnx/twelf_cm12_theme/issues

Auto Name:TwelF
Summary:CM12 theme for devices using only FLOSS apps
Description:
TwelF is a theme based on the CM12 theme engine. Your ROM must include this
NEW theme engine!

This theme contains exclusively icons for apps hosted on F-Droid FLOSS
Repository. More icons, bootanimation, ringtones, wallpapers... avaliable
on next updates.

TwelF follows the guidelines of Google Material Design for Android Lollipop
(Android 5) aiming to provide a unified and minimalistic look at devices
using CM12 with only Libre/Open Apps.

For issues, comments and icon request, please use the issue tracker.

Status: Alpha

[https://gitlab.com/xphnx/twelf_cm12_theme/blob/HEAD/CHANGELOG.md Changelog]
.

Repo Type:git
Repo:https://gitlab.com/xphnx/twelf_cm12_theme.git

Build:0.1,1
    commit=v0.1
    gradle=yes
    subdir=theme

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.1
Current Version Code:1
